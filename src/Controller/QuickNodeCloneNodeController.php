<?php

namespace Drupal\commerce_quick_node_clone\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\node\Controller\NodeController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupRelationship;

/**
 * Returns responses for Quick Node Clone Node routes.
 */
class QuickNodeCloneNodeController extends NodeController {

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer, EntityRepositoryInterface $entity_repository, AccountInterface $currentUser, ModuleHandlerInterface $moduleHandler, EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory) {
    parent::__construct($date_formatter, $renderer, $entity_repository);
    $this->currentUser = $currentUser;
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('entity.repository'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
    );
  }

  /**
   * Provides the node submission form.
   *
   * @param \Drupal\commerce_product\Entity\Product
   *   The node entity to clone.
   *
   * @return array
   *   A node submission form.
   */
  public function cloneNode(Product $node) {
    if (!empty($node)) {
      $duplicate = $node->createDuplicate();
      $duplicate->set('title', 'Clone of ' . $duplicate->label());
      $duplicate->set('changed', \Drupal::time()->getCurrentTime());
      $duplicate->save();
      $duplicate->set('uid', $this->currentUser->id());
      $duplicate->set('created', time());
      $duplicate->set('changed', time());

      // Get and store groups of original entity, if any.
      $groups = [];
      if ($this->moduleHandler->moduleExists('gnode')) {
        $relation_class = class_exists(GroupContent::class) ? GroupContent::class : GroupRelationship::class;
        /** @var \Drupal\Core\Entity\ContentEntityInterface $original_entity */
        foreach ($relation_class::loadByEntity($original_entity) as $group_relationship) {
          $groups[] = $group_relationship->getGroup();
        }
      }
      $form_state_additions['quick_node_clone_groups_storage'] = $groups;
      // Get default status value of node bundle.
      $default_bundle_status = $this->entityTypeManager->getStorage('commerce_product')->create(['type' => $duplicate->bundle()])->status->value;

      // Clone all translations of a node.
      foreach ($duplicate->getTranslationLanguages() as $langcode => $language) {
        /** @var \Drupal\node\Entity\Node $translated_node */
        $translated_node = $duplicate->getTranslation($langcode);
        $translated_node = $this->cloneParagraphs($translated_node);
        $this->moduleHandler->alter('cloned_node', $translated_node, $original_entity);

        $prepend_text = "";
        $title_prepend_config = $this->getConfigSettings('text_to_prepend_to_title');
        if (!empty($title_prepend_config)) {
          $prepend_text = $title_prepend_config . " ";
        }
        $clone_status_config = $this->getConfigSettings('clone_status');
        if (!$clone_status_config) {
          $key = $translated_node->getEntityType()->getKey('published');
          $translated_node->set($key, $default_bundle_status);
        }

        $translated_node->setTitle($this->t('@prepend_text@title',
          [
            '@prepend_text' => $prepend_text,
            '@title' => $translated_node->getTitle(),
          ],
          [
            'langcode' => $langcode,
          ]
        )
        );
      }
      $translated_node->save();
      $response = new RedirectResponse('/product/' . $translated_node->id() . '/edit');
      $response->send();
      return [];
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Clone the paragraphs of a node.
   *
   * If we do not clone the paragraphs attached to the node, the linked
   * paragraphs would be linked to two nodes which is not ideal.
   *
   * @param \Drupal\commerce_product\Entity\Product $node
   *   The node to clone.
   *
   * @return \Drupal\commerce_product\Entity\Product
   *   The node with cloned paragraph fields.
   */
  public function cloneParagraphs(Product $node) {
    foreach ($node->getFieldDefinitions() as $field_definition) {
      $field_storage_definition = $field_definition->getFieldStorageDefinition();
      $field_settings = $field_storage_definition->getSettings();
      $field_name = $field_storage_definition->getName();
      if (isset($field_settings['target_type']) && $field_settings['target_type'] == "paragraph") {
        if (!$node->get($field_name)->isEmpty()) {
          foreach ($node->get($field_name) as $value) {
            if ($value->entity) {
              $value->entity = $value->entity->createDuplicate();
              foreach ($value->entity->getFieldDefinitions() as $field_definition) {
                $field_storage_definition = $field_definition->getFieldStorageDefinition();
                $pfield_settings = $field_storage_definition->getSettings();
                $pfield_name = $field_storage_definition->getName();

                // Check whether this field is excluded and if so unset.
                if ($this->excludeParagraphField($pfield_name, $value->entity->bundle())) {
                  unset($value->entity->{$pfield_name});
                }

                $this->moduleHandler->alter('cloned_node_paragraph_field', $value->entity, $pfield_name, $pfield_settings);
              }
            }
          }
        }
      }
    }

    return $node;
  }

  /**
   * Check whether to exclude the paragraph field.
   *
   * @param string $field_name
   *   The field name.
   * @param string $bundle_name
   *   The bundle name.
   *
   * @return bool|null
   *   TRUE or FALSE depending on config setting, or NULL if config not found.
   */
  public function excludeParagraphField($field_name, $bundle_name) {
    $config_name = 'exclude.paragraph.' . $bundle_name;
    if ($exclude_fields = $this->getConfigSettings($config_name)) {
      return in_array($field_name, $exclude_fields);
    }
  }

  /**
   * Get the settings.
   *
   * @param string $value
   *   The setting name.
   *
   * @return array|mixed|null
   *   Returns the setting value if it exists, or NULL.
   */
  public function getConfigSettings($value) {
    $settings = $this->configFactory->get('quick_node_clone.settings')
      ->get($value);

    return $settings;
  }

}
