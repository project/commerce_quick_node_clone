<?php

namespace Drupal\commerce_quick_node_clone;

use Drupal\commerce_product\Entity\ProductType;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Module permissions.
 */
class CommerceQuickNodeClonePermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of permissions.
   *
   * @return array
   *   The permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function cloneTypePermissions() {
    $perms = [];

    // Generate node permissions for all node types.
    foreach (array_keys(ProductType::loadMultiple()) as $type_id) {
      $type_params = ['%type' => $type_id];
      $perms += [
        "clone $type_id content" => [
          'title' => $this->t('%type: clone content', $type_params),
        ],
      ];
    }
    return $perms;
  }

}
